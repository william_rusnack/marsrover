# MarsRover
Detroit Labs Code challenge to develop an api that moves a mars rover around on a grid.  
✮✮✮✮✮✮✮✮✮✮✮✮ LIKE IT? STAR IT! ✮✮✮✮✮✮✮✮✮✮✮✮

![](https://github.com/BebeSparkelSparkel/MarsRover/blob/master/images/preview.gif?raw=true)

I have created a interactive webpage that demonstrates the capabilities of the api that I created. You can view that by downloading this repository and opening the mars_rover_page.html file in your browser (works best in chrome and firefox, safari is supported, not tested in edge or IE).

The people that are displayed say that they work for Detroit Labs so I hope you recognize some of them.

The mars_rover_API.js and test.js (uses jasmine) took 1 hour and 45 minutes. Then the mars_rover_page.html took severl more hours, I had fun doing it though.

#### The Challenge
Develop an api that moves a rover around on a grid.  
You are given the initial starting point (x,y) of a rover and the direction (N,S,E,W) it is facing.  
The rover receives a character array of commands.  
Implement commands that move the rover forward/backward (f,b).  
Implement commands that turn the rover left/right (l,r).  
Implement wrapping from one edge of the grid to another. (planets are spheres after all)  
Implement obstacle detection before each move to a new square. If a given sequence of commands encounters an obstacle, the   rover moves up to the last possible point and reports the obstacle.  

## Mars Rover API
To use add mars_rover_API.js to your directory and import the script to your page like:
```html
<script src="mars_rover_API.js"></script>
```

#### Initialize
You now have access to the mars_rover object. To create a new instance use:
```javascript
var mars_object = new mars_rover()
```

#### Map
**make_map**(x_size, y_size, number_obstacles)  
You can create a map with randomized obstacles.  
*number_obstacles* give a probability of that many obstacles but the actual number of obstacles is not guarantee.  
```javascript
mars_object.make_map(4, 5, 3)
```
**map**  
The map can also be defined by assigning to map.
The map is an array of arrays, map[x][y].  
To have a traversable path set the location to be 'clear' and to have an obstacle any other object can be given.  
```javascript
for (var x_index = mars_object.map.length - 1; x_index >= 0; x_index--) {
  mars_object.map[x_index] = new Array(y_size)
  for (var y_index = y_size - 1; y_index >= 0; y_index--) {
      mars_object.map[x_index][y_index] = 'clear'
    }
  }
}
mars_object.map[1][2] = 'obstacle’
```

#### Rover Location
**set_rover_location**(location, cardinal_direction)  
This changes the rover location from the default of location [0,0] facing 'N' (North).  
*location* is an array [x, y]
*cardinal_direction* a string that can be 'N', 'E', 'S', or 'W' meaning North, East, South, or West.  
```javascript
mars_object.set_rover_location([2, 4], 'E')
```

#### View Map
**get_map_value**(location)  
This returns the value of the map at *location*.  
```javascript
mars_object.get_map_value([2, 3])
```

#### Move Rover
**move_rover**(command_sequence)  
This translates the rover's location or rotates the rover's direction.  
*command_sequence* a string that contains F, B, L, and/or R (forward, backward, left, right). Ex 'FLRBBR' or 'F'  
If an incorrect command character is given an error is thrown. Ex 'Unknown command: Y'  
If the rover is commanded to move onto an obstacle an error is thrown that has the obstacle location and a message. err.obstacle_location and err.message  
```javascript
try {
    mars_object.move_rover('FBLR')
} catch (err) {
    err.obstacle_location  // Example [1, 5]
    console.log(err.message)
}
```

#### Rover Status
The rover location and direction can be found by referencing the rover object.
```javascript
mars_object.rover.location  // Example [2, 3]
mars_object.rover.direction  // Example 'S'
```

#### Switch Poles
**switch_North_and_South**()  
This switches the direction of the north and south of the map incase rover is traveling the wrong direction on the map.  



✮✮✮✮✮✮✮✮✮✮✮✮ LIKE IT? STAR IT! ✮✮✮✮✮✮✮✮✮✮✮✮
