var fs = require('fs')

eval(fs.readFileSync(`${__dirname}/mars_rover_API.js`).toString())


describe('Test mars_rover.js API', function() {
  it('test making_map', function() {
    var mr = new mars_rover()
    var x_size = 5
    var y_size = 7
    var number_obstacles = 5
    mr.make_map(x_size, y_size, number_obstacles)

    expect(mr.map.length).toEqual(x_size)
    mr.map.forEach(column => expect(column.length).toEqual(y_size))

    mr.map.forEach(column => column.forEach(location =>
      expect(['obstacle', 'clear'].indexOf(location)>=0).toBe(true)
    ))
  })

  it('test correct_location', function() {
    var mr = new mars_rover()

    expect(mr.correct_location([1, 2])).toEqual([0, 0])

    mr.make_map(4, 5, 0)
    expect(mr.correct_location([1, 2])).toEqual([1, 2])
    expect(mr.correct_location([-1, -3])).toEqual([3, 2])
    expect(mr.correct_location([-9, -13])).toEqual([3, 2])
    expect(mr.correct_location([4, 6])).toEqual([0, 1])
    expect(mr.correct_location([12, 16])).toEqual([0, 1])
  })

  it('test set_rover_location', function() {
    var mr = new mars_rover()
    expect(mr.rover).toEqual({location: [0, 0], direction: 'N'})

    mr.set_rover_location([4, 6], 'S')
    expect(mr.rover).toEqual({location: [0, 0], direction: 'S'})

    mr.make_map(10, 20, 3).set_rover_location([4, 6], 'S')
    expect(mr.rover).toEqual({location: [4, 6], direction: 'S'})
  })

  it('test get_map_value', function() {
    var mr = new mars_rover()

    expect(mr.get_map_value([0,0])).toEqual('clear')
    expect(mr.get_map_value([4,59])).toEqual('clear')

    mr.map = [['a', 'b'], ['c', 'd']]
    expect(mr.get_map_value([0, 0])).toBe('a')
    expect(mr.get_map_value([1, 1])).toBe('d')
    expect(mr.get_map_value([-1, -1])).toBe('d')
    expect(mr.get_map_value([-1, 2])).toBe('c')
  })

  it('test move_rover', function() {
    var mr = new mars_rover()

    expect(function(){mr.move_rover('c')}).toThrow('Unknown command: C')

    mr.set_rover_location([0, 0]).move_rover('l')
    expect(mr.rover).toEqual({location: [0, 0], direction: 'W'})
    mr.move_rover('R')
    expect(mr.rover).toEqual({location: [0, 0], direction: 'N'})
    mr.move_rover('R')
    expect(mr.rover).toEqual({location: [0, 0], direction: 'E'})
    mr.move_rover('r')
    expect(mr.rover).toEqual({location: [0, 0], direction: 'S'})
    mr.move_rover('L')
    expect(mr.rover).toEqual({location: [0, 0], direction: 'E'})

    mr.move_rover('F')
    expect(mr.rover).toEqual({location: [0, 0], direction: 'E'})
    mr.move_rover('B')
    expect(mr.rover).toEqual({location: [0, 0], direction: 'E'})

    mr.make_map(2,3,0).move_rover('F')
    expect(mr.rover).toEqual({location: [1, 0], direction: 'E'})
    mr.move_rover('F')
    expect(mr.rover).toEqual({location: [0, 0], direction: 'E'})
    mr.move_rover('B')
    expect(mr.rover).toEqual({location: [1, 0], direction: 'E'})
    mr.move_rover('R').move_rover('F')
    expect(mr.rover).toEqual({location: [1, 2], direction: 'S'})
    mr.move_rover('B')
    expect(mr.rover).toEqual({location: [1, 0], direction: 'S'})

    mr.map[0][1] = 'big obstacle'
    mr.set_rover_location([0, 0], 'N')
    // expect(function(){mr.move_rover('f')}).toThrow('Cannot move to 0,1 because big obstacle is in the way')
    expect(function(){mr.move_rover('f')}).toThrow({
      obstacle_location: [0, 1],
      message: 'Cannot move to 0,1 because big obstacle is in the way'
    })

    mr.set_rover_location([0, 0], 'W').move_rover('LFRBLR')
    expect(mr.rover).toEqual({location: [1, 2], direction: 'W'})
  })
})
