// Develop an api that moves a rover around on a grid.
//  - You are given the initial starting point (x,y) of a rover and the direction (N,S,E,W) it is facing.
//  - The rover receives a character array of commands.
//  - Implement commands that move the rover forward/backward (f,b).
//  - Implement commands that turn the rover left/right (l,r).
//  - Implement wrapping from one edge of the grid to another. (planets are spheres after all)
//  - Implement obstacle detection before each move to a new square. If a given sequence of commands encounters an obstacle, the rover moves up to the last possible point and reports the obstacle.


// mars_rover object holds all the methods for the Detroit Labs Mars Rover code test
mars_rover = function(){
  this.map = undefined
  this.make_map(1, 1, 0)

  this.rover = {
    location: [0, 0],
    direction: 'N'
  }

  this.direction_movement = this.direction_movement
}

// sets this.map to an array array of arrays with x_size columns and y_size rows
// each location is set to 'clear' or 'obstacle' base on the probability of the random number [0,1) being less than (number_obstacles/(x_size*y_size)
mars_rover.prototype.make_map = function(x_size, y_size, number_obstacles) {
  this.map = new Array(x_size)
  for (var x_index = this.map.length - 1; x_index >= 0; x_index--) {
    this.map[x_index] = new Array(y_size)
    for (var y_index = y_size - 1; y_index >= 0; y_index--) {
      // probability to determime if obstacle is there
      if (number_obstacles/(x_size*y_size) > Math.random()) {
        this.map[x_index][y_index] = 'obstacle'
      } else {
        this.map[x_index][y_index] = 'clear'
      }
    }
  }
  return this
}

// sets the initial postion of the rover
// location - should be a 2D array [x-postion, y-position]
// cardinal_direction - optional input that sets the direction of the rover
mars_rover.prototype.set_rover_location = function(location, cardinal_direction) {
  this.rover.location = this.correct_location(location)

  if (cardinal_direction !== undefined){
    cardinal_direction = cardinal_direction.toUpperCase()
    if (this.directions.indexOf(cardinal_direction) >= 0)
      this.rover.direction = cardinal_direction
  }

  return this
}

// returns the value of the map at location
mars_rover.prototype.get_map_value = function(location) {
  location = this.correct_location(location)

  var map_val = this.map
  for (var D = 0; D < location.length; D++) {
    map_val = map_val[location[D]]
  }
  return map_val
}

// moves the rover left 'L'/'l', right 'R'/'r', forward 'F'/'f', and back 'B'/'b'
// if an unknown command character is given an exception is thrown
// if the rover hits an 'obstacle' an exception is thrown
mars_rover.prototype.move_rover = function(command_sequence) {
  for (var cs_index=0; cs_index < command_sequence.length; cs_index++) {
    command = command_sequence[cs_index].toUpperCase()

    // left or right
    if (['L', 'R'].indexOf(command) >= 0){
      if (command == 'L') var direction_change = -1
      else var direction_change = 1

      var new_direction = (this.directions.indexOf(this.rover.direction) + direction_change) % this.directions.length
      if (new_direction < 0) new_direction += this.directions.length
      this.rover.direction = this.directions[new_direction]
    }
    // forward or backward
    else if (['F', 'B'].indexOf(command) >= 0) {
      if (command == 'F') var move = 1
      else var move = -1

      var new_location = new Array(this.rover.location.length)
      for (var i = new_location.length - 1; i >= 0; i--)
        new_location[i] = this.rover.location[i] + this.direction_movement[this.rover.direction][i] * move
      if (this.get_map_value(new_location) != 'clear')
        // throw `Cannot move to ${new_location} because ${this.get_map_value(new_location)} is in the way`
        throw {
          obstacle_location: new_location,
          message: `Cannot move to ${new_location} because ${this.get_map_value(new_location)} is in the way`
        }
      this.set_rover_location(new_location)
    }
    else throw `Unknown command: ${command}`
  }

  return this
}

// holds the vector for the movement in the forward direction of the direction
// N and S can be switched to change the orientation of the poles on the map
mars_rover.prototype.direction_movement = {
  N: [0, 1],
  E: [1, 0],
  S: [0, -1],
  W: [-1, 0]
}

mars_rover.prototype.switch_North_and_South = function() {
  var tmp_N = this.direction_movement.N
  this.direction_movement.N = this.direction_movement.S
  this.direction_movement.S = tmp_N
}



// Everything below should not be used and is not part of the public API


mars_rover.prototype.directions = ['N', 'E', 'S', 'W']  // order is important

// returns the corrected location from location so that the rover stays on the map
mars_rover.prototype.correct_location = function(location) {
  var map_layer = [this.map]
  for (var D = 0; D < location.length; D++) {
    map_layer = map_layer[0]
    location[D] %= map_layer.length
    if (location[D] < 0) location[D] += map_layer.length
    if (location[D] == -0) location[D] = 0
  }
  return location
}
